//
// Created by main on 06.05.2021.
//
#include <stdio.h>
#include <omp.h>

#define PI 3.14159265358

double f(double x) {
  return 1 / (1 + x * x);
}

int main(int argc, char *argv[]) {
  double pi, sum1 = 0, sum2 = 0, term, h, t1, t2, dt;
  int myrank, nprocs, n, i;
  if (myrank == 0) {
    printf("Number of iterations=");
    scanf("%d", &n);
  }
  h = 1.0 / n;
  omp_set_num_threads(8);
  t1 = omp_get_wtime();
#pragma omp parallel for schedule(static, n/2) private(i) shared(h) reduction(+:sum1)
  for (i = 0; i < n; i++) {
    sum1 = sum1 + f(h * i);
  }
#pragma omp parallel for schedule(static, n/2) private(i) shared(h) reduction(+: sum2)
  for (i = 0; i < n + 1; i++) {
    sum2 = sum2 + f((i - 1/2)*h);
  }

//  for (i = myrank + 1; i <= n; i += nprocs)
//    sum += f(h * (i - 0.5));
  term = ((4 * h) / 3) * (((f(0) - f(1)) / 2) + 2 * sum2 + sum1);
  t2 = omp_get_wtime();
//  MPI_Reduce(&term, &pi, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
  if (myrank == 0)
    printf("pi=%f\nComputed fault of pi=%lf\nTime: %f\n", term, term - PI, t2 - t1);
//  MPI_Finalize();
  return 0;
}