//
// Created by main on 27.04.2021.
//

#include<stdio.h>
#include<stdlib.h>
#include<mpi.h>
#include<time.h>
#include <sys/time.h>
/* Задаем в каждой ветви размеры полос матриц A и B:  5х20. (Здесь предпологается, что размеры
 * полос одинаковы во всех ветвях. */
#define M 10000 
#define N 10000
/* NUM_DIMS -размер декартовой топологии.  "кольцо" - одномерный тор. */
#define DIMS 1
#define EL(x) (sizeof(x) / sizeof(x[0]))
/* Задаем полосы исходных матриц.  */
static double A[N][M], B[N], C[N];
int main(int argc, char **argv) {
  int rank, size, i, j, k, i1, d, sour, dest;
  int dims[DIMS];
  int periods[DIMS];
  int new_coords[DIMS];
  int reorder = 0;
  MPI_Comm ring;
  MPI_Status st;
  float rt, t1, t2;
/* Инициализация библиотеки MPI*/
  MPI_Init(&argc, &argv);
  /* Каждая ветвь узнает количество задач в стартовавшем приложении */
  MPI_Comm_size(MPI_COMM_WORLD, &size);
/* Обнуляем массив dims и заполняем массив periods для топологии  "кольцо" */
  for (i = 0; i < DIMS; i++) {
    dims[i] = 0;
    periods[i] = 1;
  }
/* Заполняем массив dims, где указываются размеры одномерной решетки */
  MPI_Dims_create(size, DIMS, dims);
  /* Создаем топологию "кольцо" с communicator(ом) comm_cart */
  MPI_Cart_create(MPI_COMM_WORLD, DIMS, dims, periods, reorder,
                  &ring);
  /* Каждая ветвь определяет свой собственный номер: от 0 до (size-1) */
  MPI_Comm_rank(ring, &rank);
  /* Каждая ветвь находит своих соседей вдоль кольца, в направлении меньших значений рангов */
  MPI_Cart_shift(ring, 0, -1, &sour, &dest);
  /* Каждая ветвь генерирует полосы исходных матриц A и B, полосы C обнуляет */
  for (j = 0; j < N; j++) {
    for (i = 0; i < M; i++)
      A[j][i] = rand() % 10 + 1;
    B[j] = rand() % 10 + 1;
    C[j] = 0.0;
  }
/* Засекаем начало умножения матриц */
  t1 = MPI_Wtime();
  /* Каждая ветвь производит умножение своих полос матрицы и вектора */
  /* Самый внешний цикл for(k) - цикл по компьютерам */
  for (k = 0; k < size; k++) {
    d = ((rank + k) % size) * N;
    /* Каждая ветвь производит умножение своей полосы матрицы A на текущую полосу матрицы B */
    for (j = 0; j < N; j++) {
      for (i1 = 0, i = d; i < d + N; i++, i1++)
        C[j] += A[j][i] * B[i1];
    }
    /* Каждая ветвь передает своим соседним ветвям с меньшим рангом полосы вектора B.
    /* Т.е. полосы вектора B сдвигаются вдоль кольца компьютеров */
    MPI_Sendrecv_replace(B, EL(B), MPI_DOUBLE, dest, 12, sour,
                         12, ring, &st);
  }
/* Умножение завершено. Каждая ветвь умножила свою полосу строк матрицы A на
 * все полосы вектора B.  Засекаем время и результат печатаем */
  t2 = MPI_Wtime();
  rt = t2 - t1;
  /* Для контроля печатаем первые N элементов результата */
  for (i = 0; i < N; i++)
    printf("rank = %d RM = %6.2f\n", rank, C[i]);

  printf("rank = %d Time = %1.2f\n", rank, rt);
  /* Все ветви завершают системные процессы, связанные с топологией comm_cart и
   * завершают выполнение программы */
  MPI_Comm_free(&ring);
  MPI_Finalize();
  return (0);
}
