//
// Created by main on 27.04.2021.
//

#include <omp.h>
#include<stdio.h>

#define M 10

int main() {

  float A[M][M], B[M][M], result[M][M];
  int i, j, k, rank, size;

  for (i = 0; i < M; i++) {
    for (j = 0; j < M; j++) {
      A[i][j] = (j + 1) * 1.0;
      B[i][j] = (j + 1) * 2.0;
      result[i][j]=0;
    }
  }

  printf("Вывод значений матрицы A и вектора b на экран:\n");

  for (i = 0; i < M; i++) {
    printf(" A[%d]= ", i);
    for (j = 0; j < M; j++)
      printf("%.1f ", A[i][j]);
    printf("\n");
  }

  printf("\n");

  for (i = 0; i < M; i++) {
    printf(" B[%d]= ", i);
    for (j = 0; j < M; j++)
      printf("%.1f ", B[i][j]);
    printf("\n");
  }

  omp_set_num_threads(2);
#pragma omp parallel for shared(A, B, result) private(i, j, k)
    for (i = 0; i < M; i++) {
      for (j = 0; j < M; j++) {
        result[i][j] = 0;
        for (k = 0; k < M; k++) {
          result[i][j] += (A[i][k] * B[k][j]);
        }
      }
    }

  printf("\n");

  for (i = 0; i < M; i++) {
    printf(" C[%d]= ", i);
    for (j = 0; j < M; j++)
      printf("%.1f ", result[i][j]);
    printf("\n");
  }
  return 0;
}

